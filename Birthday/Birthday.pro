#-------------------------------------------------
#
# Project created by QtCreator 2018-12-23T20:16:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Birthday
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    birthday.cpp \
    people.cpp \
    table.cpp

HEADERS  += mainwindow.h \
    birthday.h \
    people.h \
    table.h

FORMS    += mainwindow.ui \
    table.ui

RESOURCES += \
    resources/actions.qrc
