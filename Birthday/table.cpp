#include "table.h"
#include "ui_table.h"
#include <QFile>
#include <QString>
#include <QTextStream>
#include <QIODevice>
#include <QTableWidgetItem>
#include <QMessageBox>
#include <QDebug>

Table::Table(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Table)
{
    ui->setupUi(this);
    setWindowTitle("Table");
}

Table::~Table()
{
    delete ui;
}

void Table::WrittingInTable( int sizeRow, QString &nameFile)
{
    QFile iFile(nameFile);

    if( !iFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::warning(this, tr("Warning"),
                             tr("file was not oppened"), QMessageBox::Yes,QMessageBox::Cancel);
        return;
    }

    if( nameFile == QString("Congratulate.txt"))
        setWindowTitle("Today birthday");
    else
        setWindowTitle("List of persons");

    ui->tableWidget->setRowCount(sizeRow);

    QTextStream fileStream (&iFile);
    QString strName, strDay, strMonth,strYear, strNumber;
    for(int i = 0; i < sizeRow; i++)
    {
        int j = 0;
        strName = fileStream.readLine();
        strDay = fileStream.readLine();
        strMonth = fileStream.readLine();
        strYear = fileStream.readLine();
        strNumber = fileStream.readLine();


        QString strBirthday = strDay + "/" + strMonth + "/" + strYear;
        QTableWidgetItem *cell1 = new QTableWidgetItem;
        ui->tableWidget->setItem(i,j,cell1);
        cell1->setText(strName);

        QTableWidgetItem *cell2 = new QTableWidgetItem;
        ui->tableWidget->setItem(i,++j,cell2);
        cell2->setText(strBirthday);

        QTableWidgetItem *cell3 = new QTableWidgetItem;
        ui->tableWidget->setItem(i,++j,cell3);
        cell3->setText(strNumber);
    }
    iFile.close();
}
