#ifndef TABLE_H
#define TABLE_H

#include "birthday.h"
#include "people.h"
#include <QDialog>
//#include <QMap>
//#include <QString>
//#include <QTableWidget>

namespace Ui {
class Table;
}

class Table : public QDialog
{
    Q_OBJECT

public:
    explicit Table(QWidget *parent = 0);
    ~Table();

private:
    Ui::Table *ui;
//    QTableWidget* mTableWidget;
public:
    void WrittingInTable(int sizeRow, QString &nameFile);
};

#endif // TABLE_H
