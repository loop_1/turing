#ifndef PEOPLE_H
#define PEOPLE_H

#include <QString>
#include "birthday.h"

class People
{
public:
    People();
    ~People();
    People(const People &obj);
    People(Birthday data,QString name,int number);
    People& set_number(int number);
    People& set_name(QString name);
    QString get_name() const;
    People& set_data( short ,short,unsigned int);
    int get_number() const;
    People& full_info();
    Birthday get_data() const;
    People operator = ( const People &obj2);
    friend bool operator ==(const People &obj1 , const People &obj2 );
    friend bool operator !=(const People &obj1 , const People &obj2 );
    friend bool operator >(const People &obj1 , const People &obj2 );
    friend bool operator <(const People &obj1 , const People &obj2 );
    friend QDebug operator << (QDebug dbg, const People &obj);
//     friend void operator <<(QTextStream str,People&obj);
//     friend QDebug operator >> (QDebug dbg, const People &obj);
private:
    QString name;
    double number;
    Birthday data;
};

#endif // PEOPLE_H
