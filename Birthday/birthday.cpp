#include "birthday.h"
#include <QDebug>

Birthday::~Birthday()
{
//    cout<<"I am a destruktor Birthday";
}

Birthday::Birthday(short day,short month,unsigned int year)
{
    this->day=day;
    this->month=month;
    this->year=year;
}

Birthday::Birthday(const Birthday &obj)
{
    this->day = obj.day;
    this->month = obj.month;
    this->year = obj.year;
}

Birthday Birthday:: operator =(const Birthday&obj)
{
    day = obj.day;
    month = obj.month;
    year = obj.year;
    return *this;
}

Birthday& Birthday::set_day(short day)
{
    this->day=day;
    return *this;
}

Birthday& Birthday::set_month(short month)
{
    this->month=month;
    return *this;
}

Birthday& Birthday::set_year(unsigned int year)
{
    this->year=year;
    return *this;
}

short Birthday::get_day() const
{
    return this->day;
}

short Birthday::get_month() const
{
    return this->month;
}

unsigned int Birthday::get_year() const
{
    return this->year;
}

bool operator ==( const Birthday &obj1, const Birthday &obj2)
{
    if((obj1.month==obj2.month)&&(obj1.day)==(obj2.day) &&(obj1.month)==(obj2.month))
         return true ;

    return false;
}

bool operator !=(const Birthday &obj1, const Birthday &obj2)
{
    if(!((obj1.day)==obj2.day && (obj1.month)==obj2.month))
        return false;

    return true;
}

bool operator >(const Birthday &obj1, const Birthday &obj2)
{
     if((obj1.month)>=obj2.month)
        if(obj1.day>obj2.day)
          return true;

    return false;
}

bool operator <(const Birthday &obj1, const Birthday &obj2)
{
    if((obj1.month)<=obj2.month)
       if(obj1.day<obj2.day)
         return true;

    return false;
}

QDebug operator<<(QDebug dbg, const Birthday &obj)
{
    dbg<<"Day of birth : "<<obj.day<<" "<<"Month of birth : "<<obj.month<<" "<<"Year of birth : "<<obj.year<<endl;
    return dbg;
}

