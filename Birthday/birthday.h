#ifndef BIRTHDAY_H
#define BIRTHDAY_H

#include <QTextStream>

class Birthday
{
public:
    ~Birthday();
    Birthday(short day = 0,short month = 0,unsigned int year = 0);
    Birthday (const Birthday &obj);
    Birthday operator =(const Birthday &obj1);
    friend bool operator ==(const Birthday &obj1 , const Birthday &obj2 );
    friend bool operator !=(const Birthday &obj1 , const Birthday &obj2 );
    friend bool operator >(const Birthday &obj1 , const Birthday &obj2 );
    friend bool operator <(const Birthday &obj1 , const Birthday &obj2 );
    friend QDebug operator << (QDebug dbg, const Birthday &obj);
    friend QTextStream operator<<(QTextStream stream , Birthday &obj);
    short get_month() const;
    short get_day() const;
    unsigned int get_year() const;
    Birthday& set_month(short month);
    Birthday& set_day(short day);
    Birthday& set_year(unsigned int year);
private:
    short day;
    short month;
    unsigned int year;
};

#endif // BIRTHDAY_H
