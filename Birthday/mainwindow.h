#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QMap>
#include "birthday.h"
#include "people.h"
#include "table.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QMap <QString,People> iMap;
    Table iTable;

public:
    void LineEditEmptyWarning( QString &);
private:
    void InsertMap();
    void ApendInFile();
    void WriteOnlyFile();
    bool DoubleIncomingPerson(People &doublePeople);
private slots:
        void slotSearchPerson();
        void slotAboutProgram();
        void slotAddMember();
        void slotTable();
        void slotDeleteMembers();
        void slotOpenFile();
        void slotAddDate();
        void slotCongratulate();
        void slotMainRules();
};

#endif // MAINWINDOW_H
