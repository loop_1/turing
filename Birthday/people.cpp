#include "people.h"
#include <QDebug>

People::People()
{
    number=380988888888;
    name="Anonim";
}

People::People(Birthday data,QString name,int number)
{
    this->data = data;
    this->name = name;
    this->number = number;
}

People::~People()
{
  //  cout<<"I am a destruktor People"<<endl;
}

People::People(const People &obj)
{
    number = obj.number;
    data = obj.data;
    name = obj.name;
}

People People:: operator =( const People &obj2)
{
    data = obj2.data;
    number = obj2.number;
    name = obj2.name;
    return *this;
}

bool operator ==(const People &obj1,const People &obj2)
{
    qDebug() << "comparePeople1";
    if(obj1.data == obj2.data
            && obj1.name == obj2.name && obj1.number == obj2.number)
        return true;
    qDebug() << "comparePeople2";
    return false;
}

bool  operator >(const People &obj1,const People &obj2)
{
    if(obj1.data>obj2.data)
        return true;

    return false;
}

bool operator <(const People &obj1,const People &obj2)
{
    if(obj1.data<obj2.data)
        return true;

    return false;
}

bool  operator != (const People &obj1,const People &obj2)
{
    if(!(obj1.data==obj2.data))
        return true;

    return false;
}

People& People::set_number(int number)
{
    this->number=number;
    return *this;
}

People& People::set_name(QString name)
{
    this->name=name;
    return *this;
}

int People::get_number() const
{
    return this->number;
}

QString People::get_name() const
{
    return this->name;
}

Birthday People::get_data() const
{
    return this->data;
}

QDebug operator<<(QDebug dbg, const People &obj)
{
    dbg<<"First and Last name : "<< obj.get_name() <<
       "  Birthday:  " << obj.get_data() <<
         " Number of phone:  "<< obj.get_number() << endl;
    return dbg;
}

People& People::set_data( short day,short month, unsigned int year)
{
    this->data.set_day(day);
    this->data.set_month(month);
    this->data.set_year(year);
    return *this;
}
