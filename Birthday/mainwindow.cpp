#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "table.h"
#include <QMessageBox>
#include <QDate>
#include <QDir>
#include <QLineEdit>
#include <QValidator>
#include <QInputDialog>
#include <QMapIterator>
#include <QDebug>
#include <QIODevice>
#include <QApplication>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Forfotten Birthday");
    ui->action_Quit->setShortcut(QKeySequence::Quit);
    ui->action_Open->setShortcut(QKeySequence::Open);

//    QFile f;
//    f.link(QApplication::applicationDirPath(), QApplication::applicationDirPath() + "/link.ink");

    ui->lineEdit_6->setPlaceholderText("First and Last name");
    ui->lineEdit_3->setPlaceholderText("day");
    ui->lineEdit_5->setPlaceholderText("years");
    ui->lineEdit_4->setPlaceholderText("mouth");
    ui->lineEdit_2->setPlaceholderText("Number of telefone");

    ui->action_AboutProgram->setIcon(QIcon(":/actions/About"));
    ui->action_DeleteData->setIcon(QIcon(":/actions/DeleteMembers"));
    ui->action_AddData->setIcon(QIcon(":/actions/AddMembers"));
    ui->action_AddPesron->setIcon(QIcon(":/actions/AddPerson"));
    ui->action_Quit->setIcon(QIcon(":/actions/Exit"));
    ui->action_Table->setIcon(QIcon(":/actions/Table"));
    ui->action_Open->setIcon(QIcon(":/actions/Open"));
    ui->action_Search_Person->setIcon(QIcon(":/actions/SearchPerson"));
    ui->action_Congratulate->setIcon(QIcon(":/actions/Congradulate"));

    //Memory
    QValidator *mIntValidatorDay = new QIntValidator (0,31,this);
    QValidator *mIntValidatorMonth = new QIntValidator (0,12,this);
    QValidator *mIntValidatorYear = new QIntValidator (0,2200,this);
    QValidator *mIntValidatorNumber = new QIntValidator (0,1000000000,this);

    ui->lineEdit_3->setValidator(mIntValidatorDay);
    ui->lineEdit_4->setValidator(mIntValidatorMonth);
    ui->lineEdit_5->setValidator(mIntValidatorYear);
    ui->lineEdit_2->setValidator(mIntValidatorNumber);

    connect(ui->action_DeleteData,SIGNAL(triggered()), this, SLOT(slotDeleteMembers()), Qt::UniqueConnection);
    connect(ui->action_AddPesron,SIGNAL(triggered()), this, SLOT(slotAddMember()), Qt::UniqueConnection);
    connect(ui->action_Table,SIGNAL(triggered()), this, SLOT(slotTable()), Qt::UniqueConnection);
    connect(ui->action_AboutProgram,SIGNAL(triggered()), this, SLOT(slotAboutProgram()), Qt::UniqueConnection);
    connect(ui->pushButton,SIGNAL(clicked()), this, SLOT(slotAddMember()), Qt::UniqueConnection);
    connect(ui->action_Open, SIGNAL(triggered()),this,SLOT(slotOpenFile()),Qt::UniqueConnection);
    connect(ui->action_AddData, SIGNAL(triggered()),this,SLOT(slotAddDate()),Qt::UniqueConnection);
    connect(ui->action_Search_Person, SIGNAL(triggered()),this,SLOT(slotSearchPerson()),Qt::UniqueConnection);
    connect(ui->action_Congratulate, SIGNAL(triggered()),this,SLOT(slotCongratulate()),Qt::UniqueConnection);
    connect(ui->action_The_main_rules, SIGNAL(triggered()),this,SLOT(slotMainRules()),Qt::UniqueConnection);

    Table iTable(this);
}

MainWindow::~MainWindow()
{
   iTable.close();
    delete ui;
}

void MainWindow::slotAboutProgram()
{
    QMessageBox::about(this,tr("Information abou program"), QString("Program was created for you!"));
}

void MainWindow::slotAddMember()
{
    QString StringName = ui->lineEdit_6->displayText();
    if (StringName.isEmpty())
    {
        QString war("Enter line of $First and Last name$");
        LineEditEmptyWarning(war);
        return;
    }

    QString StringDay = ui->lineEdit_3->displayText();
    if (StringDay.isEmpty())
    {
        QString war("Enter line of $Day$");
        LineEditEmptyWarning(war);
        return;
    }

    QString StringMonth = ui->lineEdit_4->displayText();
    if (StringMonth.isEmpty())
    {
        QString war("Enter line of $Month$");
        LineEditEmptyWarning(war);
        return;
    }
    QString StringYear = ui->lineEdit_5->displayText();
    if (StringYear.isEmpty())
    {
        QString war("Enter line of $Year$");
        LineEditEmptyWarning(war);
        return;
    }
    QString StringNumberPhone = ui->lineEdit_2->displayText();
    if (StringNumberPhone.isEmpty())
    {
        QString war("Enter line of $Number Telefone$");
        LineEditEmptyWarning(war);
        return;
    }

    Birthday ObjBirthday ( StringDay.toShort(),StringMonth.toShort(),StringYear.toInt());
    People ObjPeople (ObjBirthday, StringName, StringNumberPhone.toInt());

    if( !DoubleIncomingPerson(ObjPeople))
        iMap.insertMulti(StringName,ObjPeople);

    qDebug() << ObjBirthday << ObjPeople;

    QString name("Person was added");
    LineEditEmptyWarning(name);
    return;
}

void MainWindow::slotTable()
{    
    if (!iMap.empty())
        ApendInFile();
    InsertMap();

    QString nameFile("List.txt");
    iTable.WrittingInTable(iMap.size(), nameFile);
    iTable.show();

    if(!iMap.empty())
        iMap.clear();
}

void MainWindow::slotDeleteMembers()
{
    int iResult = QMessageBox::question(this, tr("Dlete people"),
                         QString(tr("Do you wand delete people?")),
                         QMessageBox::Yes, QMessageBox::No,QMessageBox::Cancel);
    if(QMessageBox::No == iResult || QMessageBox::Cancel == iResult)
        return;

    if( !iMap.isEmpty())
        ApendInFile();
    InsertMap();

    QString StrDelName;
    bool ok;
    QString StringText = QInputDialog::getText(this,tr("Delete person"),
                                tr("Enter the name person: "), QLineEdit::Normal,StrDelName,&ok);
    if( ok && !StringText.isEmpty())
        StrDelName = StringText;

    if(iMap.find(StrDelName) != iMap.end())
        iMap.erase(iMap.find(StrDelName));
    else
    {
        QString message("Person was not found");
        LineEditEmptyWarning(message);
    }

    WriteOnlyFile();
    qDebug() << StrDelName;
}

void MainWindow::slotOpenFile()
{
    qDebug() << "OKKKKKKKKKKKK";
    QMapIterator<QString,People> MupIter(iMap);
    while( MupIter.hasNext() )
    {
        MupIter.next();
        qDebug() << MupIter.key() << MupIter.value();
    }
}

void MainWindow::LineEditEmptyWarning( QString &StringText)
{
    QMessageBox::warning(this, tr("Warning"),
                         StringText, QMessageBox::Yes,QMessageBox::Cancel);
}

void MainWindow::slotAddDate()
{
    ApendInFile();
}

void MainWindow::InsertMap()
{
    QFile mFile("List.txt");
    if (!mFile.open(QIODevice::ReadOnly |QIODevice::Text) )
    {
        QString message("File was not opened");
        LineEditEmptyWarning(message);
        return;
    }
    QString StringName, StringDay,
            StringMonth, StringYears,StringNumber;

    People objPeople;
    QTextStream streamFile(&mFile);
    while( !streamFile.atEnd() )
    {
        StringName = streamFile.readLine();
        StringDay = streamFile.readLine();
        StringMonth = streamFile.readLine();
        StringYears = streamFile.readLine();
        StringNumber = streamFile.readLine();

        objPeople.set_name(StringName);
        objPeople.set_number(StringNumber.toInt());
        objPeople.set_data(StringDay.toInt(), StringMonth.toInt()
                           , StringYears.toInt());

        iMap.insertMulti(StringName,objPeople);
    }
}

void MainWindow::ApendInFile()
{
    QFile mFile("List.txt");
    if (!mFile.open(QIODevice::Append |QIODevice::Text) )
    {
        QString message("File was not opened");
        LineEditEmptyWarning(message);
        return;
    }

    QTextStream streamFile(&mFile);
    QMapIterator<QString,People> iterMap(iMap);
    while( iterMap.hasNext())
    {
        iterMap.next();
        streamFile << iterMap.value().get_name() << endl
               << iterMap.value().get_data().get_day() << endl
               << iterMap.value().get_data().get_month() << endl
               << iterMap.value().get_data().get_year() << endl
               << iterMap.value().get_number()<< endl;
    }
    mFile.close();
    iMap.clear();
}

void MainWindow::WriteOnlyFile()
{
    QFile mFile("List.txt");
    if (!mFile.open(QIODevice::WriteOnly |QIODevice::Text) )
    {
        QString message("File was not opened");
        LineEditEmptyWarning(message);
        return;
    }

    QTextStream streamFile(&mFile);
    QMapIterator<QString,People> iterMap(iMap);
    while( iterMap.hasNext())
    {
        iterMap.next();
        streamFile << iterMap.value().get_name() << endl
               << iterMap.value().get_data().get_day() << endl
               << iterMap.value().get_data().get_month() << endl
               << iterMap.value().get_data().get_year() << endl
               << iterMap.value().get_number()<< endl;
    }
    mFile.close();
    iMap.clear();
}

void MainWindow::slotSearchPerson()
{
    QString StrSearchName;
    bool ok;
    QString StringText = QInputDialog::getText(this,tr("Search person"),
                                tr("Enter the name person: "), QLineEdit::Normal,StrSearchName,&ok);
    if( ok && !StringText.isEmpty())
        StrSearchName = StringText;
    if( !iMap.empty() )
        ApendInFile();
    InsertMap();

    if(iMap.find(StrSearchName) != iMap.end())
    {

       QMap<QString, People>::iterator iterMap = iMap.find(StrSearchName);
       QString StringDay = QString::number(iterMap.value().get_data().get_day());
       QString StringMonth = QString::number(iterMap.value().get_data().get_month());
       QString StringYears = QString::number(iterMap.value().get_data().get_year());
       QString StringNumber = QString::number(iterMap.value().get_number());
       QString informString = "Name: " + iterMap.key() + "\nBirthday: "+ StringDay
               + "/" + StringMonth + "/" + StringYears
               + "\nNumber:" + StringNumber;

       QMessageBox::information(this, tr("Inform about person"),
                            informString, QMessageBox::Yes,QMessageBox::Cancel);
    }
    else
    {
        QString message("Person was not found");
        LineEditEmptyWarning(message);
    }
}

void MainWindow::slotCongratulate()
{
    qDebug() << "Congratulate";
    QDate todayDate = todayDate.currentDate();
    Birthday objBirthday(todayDate.day(),todayDate.month(),todayDate.year());

    if( iMap.empty())
        ApendInFile();
    InsertMap();

    QMapIterator<QString,People> iterMap(iMap);
    QMap<QString, People> tempMap;
    while( iterMap.hasNext())
    {
        iterMap.next();
        if(iterMap.value().get_data().get_day() == objBirthday.get_day() &&
                iterMap.value().get_data().get_month() == objBirthday.get_month())
        {
            Birthday tempBirthday (iterMap.value().get_data().get_day(),
                                  iterMap.value().get_data().get_month(),
                                   iterMap.value().get_data().get_year());
            People tempPeople (tempBirthday,iterMap.key(), iterMap.value().get_number() );
            tempMap.insertMulti(iterMap.key(),tempPeople);
        }
    }

    QFile iFile("Congratulate.txt");
    if (!iFile.open(QIODevice::WriteOnly |QIODevice::Text) )
    {
        QString message("File was not opened");
        LineEditEmptyWarning(message);
        return;
    }

    QTextStream streamFile(&iFile);
    QMapIterator<QString,People> iterMap1(tempMap);
    while( iterMap1.hasNext())
    {
        iterMap1.next();
        streamFile << iterMap1.value().get_name() << endl
               << iterMap1.value().get_data().get_day() << endl
               << iterMap1.value().get_data().get_month() << endl
               << iterMap1.value().get_data().get_year() << endl
               << iterMap1.value().get_number()<< endl;
    }
    iFile.close();

    QString nameFile("Congratulate.txt");
    iTable.WrittingInTable(tempMap.count(), nameFile);
    iMap.clear();
    iTable.show();
}

bool MainWindow::DoubleIncomingPerson(People &doublePeople)
{
    QFile iFile("List.txt");
    if(!iFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QString message("File was not opened");
        LineEditEmptyWarning(message);
        return false;
    }

    QTextStream streamFile (&iFile);
    QString strName, strDay, strMonth, strYear, strNumber;
    while (!streamFile.atEnd())
    {
        strName = streamFile.readLine();
        strDay = streamFile.readLine();
        strMonth = streamFile.readLine();
        strYear = streamFile.readLine();
        strNumber = streamFile.readLine();

        if(strName == doublePeople.get_name() &&
                strDay.toInt() == doublePeople.get_data().get_day() &&
                strMonth.toInt() == doublePeople.get_data().get_month() &&
                strYear.toInt() == (int)doublePeople.get_data().get_year() &&
                strNumber.toInt() == doublePeople.get_number())
        {
            qDebug() << "findDoubleInComming";
            return true;
        }
    }

    if(iMap.find(strName) != iMap.end())
        iMap.erase(iMap.find(strName));
    return false;
}

void MainWindow::slotMainRules()
{
    qDebug() << "Rules";
    QFile mFile("Rules.txt");

    if (!mFile.open(QIODevice::ReadOnly |QIODevice::Text) )
    {
        QString message("File was not opened");
        LineEditEmptyWarning(message);
        return;
    }
    QString StringRules;

    QTextStream streamFile(&mFile);
    while( !streamFile.atEnd() )
        StringRules += streamFile.readLine() + "\n";
    QMessageBox::information(this, tr("Rules"),
                         StringRules, QMessageBox::Ok,QMessageBox::Cancel);
}
